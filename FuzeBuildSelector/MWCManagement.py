from PySide2.QtWidgets import QMessageBox
from PySide2.QtGui import QIcon

# Initial Variables
# MWC dictionary for in office use
#mwcNumbers = {}

# MWC dictionary for testing
mwcNumbers = {}


class MWCManagementWindow():
    def __init__(self):

        msgBox = QMessageBox()
        msgBox.setWindowIcon(QIcon('runningman.png'))
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setWindowTitle("Under Construction")
        msgBox.setText("This functionality is not yet completed!")
        msgBox.exec_()
