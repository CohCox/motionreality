"""
    Script to zip up a chosen directory, modify output_filename and dir_name as needed
    For use with the FuzeBuildSelector script.
    TODO = Come up with a better progress solution while file is zipping. There is a deficiency in using the zipfile module that does not allow tracking of
        transferred bytes easily. The current implementation is a pop-up window with a title of "Zipping the file....." it will hang on the screen until another
        pop-up occurs sifnifying that the zip is completed.
        
"""

import os
import zipfile
from PySide2.QtWidgets import QMessageBox

# Hard-coded destination path for zip, MUST ALTER FOR SPECIFC SYSTEM
# This is the REAL path!!!! Uncomment this when moved to the release volume!

#output_filename = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze.zip"
#dir_name = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze"

# Local testing variables, alter these for testing but comment out when in the volume
output_filename = 'C:/Users/Cody/Desktop/fuze_staging_parent/fuze.zip'
dir_name = 'C:/Users/Cody/Desktop/fuze_staging_parent/fuze'


def directoryZip():
    #output_filename = 'C:/Users/Cody/Desktop/fuze_staging_parent/fuze.zip'
    #dir_name = 'C:/Users/Cody/Desktop/fuze_staging_parent/fuze'

    if not os.path.exists(dir_name):
        nofuzeWarning()
    # Check if zipfile already exists locally
    elif not os.path.exists(output_filename):
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))

    # Begin zipping process
    elif os.path.exists(output_filename):
        os.remove(output_filename)
        # Run zipping function to get a fuze.zip
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))

# Zipping messages


def NeverReachError(self):
    msgBox = QMessageBox()
    msgBox.setWindowTitle("Problem")
    msgBox.setIcon(QMessageBox.Critical)
    msgBox.setText("There is an issue, contact the software team")
    msgBox.exec_()


def zippingMessage():
    msgBox = QMessageBox()
#    msgBox.setWindowIcon(QIcon('runningman.png'))
    msgBox.setIcon(QMessageBox.Question)
    msgBox.setWindowTitle("File Zip")
    msgBox.setText(
        "This will create a zip file of the chosen build for copying to MWCs")
    msgBox.setInformativeText(
        "Would you like to create a zip of the build? (There will be a slight lag during zip process if yes)")
    msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    msgBox.setDefaultButton(QMessageBox.Yes)

    ret = msgBox.exec_()

    # Successful link creation
    if ret == QMessageBox.Yes:
        zipProgressMessage()

    elif ret == QMessageBox.No:
        return

    # If an error gets to this point, review the code because it should never reach this check
    else:
        NeverReachError()


def zipProgressMessage():
    msgBox = QMessageBox()
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setWindowTitle("Zipping the file.....")
    msgBox.setText("Zipping the file...")
    msgBox.show()
    directoryZip()
    msgBox.close()
    zipDone()


def zipDone():
    msgBox = QMessageBox()
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setWindowTitle("Zip File")
    msgBox.setText("Zip Complete")
    msgBox.show()
    msgBox.exec_()


def nofuzeWarning():
    msgBox = QMessageBox()
    msgBox.setText(
        "No Fuze shortcut exists, create one using the link function of this program")
    msgBox.show()
    msgBox.exec_()


# Main
if __name__ == '__main__':
    directoryZip()
