import sys, ctypes, os, errno, getpass, zipfile, paramiko, tqdm, time
from paramiko import SSHClient, SFTPClient
from scp import SCPClient
from os.path import basename

# Source path
localDirec = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze.zip'

ip = ''
username = ''
password = ''
remoteDirec = ''
zipRemote = ''
mvPath = ''
rmPath = ''
totalPath = ''

#Performs Copy of build over SSH to remote destination
def RunSCP():
    #Set up SSH client into remote destination
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname=ip, username=username, password=password)
        sftp = ssh.open_sftp()
        sftp.chdir(remoteDirec)
        
        #Function for progress status
        def printTotals(transferred, toBeTransferred):
            print("Transferred: {0}\tOut of: {1}".format(transferred, toBeTransferred))
        def buildCopy():
            print("Copying over build...")
            sftp.put(localDirec, zipRemote, callback=printTotals)      
            print("Unzipping file...")
            ssh.exec_command("unzip " + zipRemote + " -d " + remoteDirec)
            hooby = input("> ")
            ssh.exec_command("mv " + mvPath + " " + remoteDirec)
            hooby = input("> ")
            ssh.exec_command("rm -r " + rmPath)
            hooby = input("> ")
            sftp.remove(zipRemote)

        #Check if zip file already exists on pack, if so delete it
        try:
            print(sftp.stat(zipRemote))
            sftp.remove(zipRemote)
            buildCopy()
            if os.path.exists("/home/pi/Desktop/fuze/fuze/hooby.txt"):
                print("Build Successful")
            else:
                print("Contact software team")
        
        except:
            pass
        
        #Check if unzipped build folder already present on remote destination, if so delete and proceed THIS DOES NOT FUNCTION YET!!!!
        try:
            #totalPath = remoteDirec + "/fuze"
            print(sftp.stat(totalPath))
            ssh.exec_command("rm -r " + totalPath)
            
            buildCopy()
                
        #If no issues, copies zip file to remote destination, unzips and then deletes the zip file
        except IOError:
            buildCopy()
            
        sftp.close()
        ssh.close()
    except paramiko.SSHException:
        print('Connection Error')
        chubby = input("Press any key to close...")
        sftp.close()