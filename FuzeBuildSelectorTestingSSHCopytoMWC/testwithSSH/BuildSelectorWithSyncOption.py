""" 
FUZE BUILD SELECTOR for use with Dauntless Fuze
Bug where after creation of link, the warning of the path existing does not show up in you select same folder again
"""
##To Create .exe
#       1.from the terminal cd into the same directory this script is in
#       2.then type pyinstaller --onedir --windowed --icon=run.ico FuzeBuildSelector.py
#         --icon=run.ico is optional to give the application an icon, place the runningman.png into the folder holding the .exe to have the window icon
#         Utilize --onedir instead of --onefile as it greatly increases the start up speed of the application, trials averaged 15 seconds to start up with --onefile vs 4 seconds with --onedir 
import sys, ctypes, os, errno, getpass, zipfile, paramiko, tqdm, time
from paramiko import SSHClient, SFTPClient
from scp import SCPClient
from os.path import basename
import fuzeZip, secureCopy

from PySide2.QtCore import Qt, QSize, Signal
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (
    QApplication,
    QHBoxLayout,
    QFormLayout,
    QLabel,
    QMainWindow,
    QPushButton,
    QStackedLayout,
    QVBoxLayout,
    QWidget,
    QFileDialog,
    QLineEdit,
    QMessageBox,
    QComboBox,
    QFrame,
    QCheckBox,
)

# Check if admin
def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

#Initial Variables
mwcNumbers = {'MWC1':'192.168.254.66',
            'MWC2': '192.168.254.64'
                }
class MWCManagementWindow(QWidget):
    def __init__(self):
        super().__init__()
        
        self.mwcNumbers = mwcNumbers
        
        self.setWindowTitle("MWC Management")
        self.setFixedSize(400, 150)
        self.pageLayout = QHBoxLayout()
        self.vLayout1 = QVBoxLayout()
        self.vLayout2 = QVBoxLayout()
        self.pageLayout.addLayout(self.vLayout1)
        self.pageLayout.addLayout(self.vLayout2)
        
        self.vLayout1.addStretch()
        
        self.label = QLabel("Choose MWCs")
        self.vLayout1.addWidget(self.label)
        
        # MWC Checkboxes
        self.checkboxes = {}
        for label in mwcNumbers:
            checkbox = QCheckBox(label)
            checkbox.setCheckState(Qt.Unchecked)
            self.vLayout1.addWidget(checkbox)
            self.checkboxes[label] = checkbox
        
        self.vLayout1.addStretch()
        self.vLayout2.addStretch()
        
        # Buttons
        btn = QPushButton("Select All")
        btn.pressed.connect(self.select_all_pressed)
        self.vLayout2.addWidget(btn)
        
        btn = QPushButton("Clear Selections")
        btn.pressed.connect(self.deselect_all_pressed)
        self.vLayout2.addWidget(btn)
        
        btn = QPushButton("Push to MWCs")
        btn.pressed.connect(self.push_to_mwc_pressed)
        self.vLayout2.addWidget(btn)  
        self.vLayout2.addStretch()
        
        self.setLayout(self.pageLayout)
            
    def select_all_pressed(self):
        for label, checkbox in self.checkboxes.items():
            checkbox.setCheckState(Qt.Checked)
    def deselect_all_pressed(self):
        for label, checkbox in self.checkboxes.items():
            checkbox.setCheckState(Qt.Unchecked)
    def push_to_mwc_pressed(self):
        # for loop to run through checkboxes 
        # set bool to true if checked
        # if bool is false cause warning box
        # Check if you can ping the ip
        for label, checkbox in self.checkboxes.items():
            if checkbox.checkState() and label in mwcNumbers:
                ip = mwcNumbers[label]
                if ip == '192.168.254.66':
                    print("This is the Pi")
                    secureCopy.ip = '192.168.254.66'
                    secureCopy.username = 'pi'
                    secureCopy.password = '1welcome2'
                    secureCopy.remoteDirec = '/home/pi/Desktop/fuze'
                    secureCopy.zipRemote = '/home/pi/Desktop/fuze/fuze.zip'
                    secureCopy.mvPath = '/home/pi/Desktop/fuze/Desktop/Fuze_Staging_Parent/fuze'
                    secureCopy.rmPath = '/home/pi/Desktop/fuze/Desktop'
                    secureCopy.totalPath = '/home/pi/Desktop/fuze/fuze'
                    secureCopy.RunSCP()
                
                if ip == '192.168.254.64':
                    print("This is the Mac")
                    secureCopy.ip = '192.168.254.64'
                    secureCopy.username = 'CohCox'
                    secureCopy.password = '1welcome2'
                    secureCopy.remoteDirec = '/Users/CohCox/Desktop/fuze'
                    secureCopy.zipRemote = '/Users/CohCox/Desktop/fuze/fuze.zip'
                    secureCopy.mvPath = '/Users/CohCox/Desktop/fuze/Desktop/Fuze_Staging_Parent/fuze'
                    secureCopy.rmPath = '/Users/CohCox/Desktop/fuze/Desktop'
                    secureCopy.totalPath = '/Users/CohCox/Desktop/fuze/fuze'
                    secureCopy.RunSCP()

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        
        # Window Title and Icon
        self.setWindowTitle("Fuze Build Selector")
        self.setFixedSize(QSize(400, 90))        
        self.setIcon()
        
        # Basic Page Layout
        pagelayout = QVBoxLayout()
        hLayout0 = QHBoxLayout()
        hLayout1 = QHBoxLayout()
        hLayout2 = QHBoxLayout()
        hLayout3 = QHBoxLayout()    
        pagelayout.addLayout(hLayout0)
        pagelayout.addLayout(hLayout1)
        pagelayout.addLayout(hLayout2)
        self.stacklayout = QStackedLayout()  
        pagelayout.addLayout(self.stacklayout)
        
        # Hard-coded destination path for link, MUST ALTER FOR SPECIFC SYSTEM
        #This is the REAL path!!!! Uncomment this when moved to the release volume! Will also need to be changed under run_symlink method!!! Line 163
        #dst = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze"
        # For use when testig outside of volume. Add in whatever path you'd like to test at here and comment out the path above
        dst = "C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze"
        
        # Horizontal Box Layout 0 (Text Label)
        label = QLabel("Select A Build")
        hLayout0.addWidget(label)
        
        # Horizontal Box Layout 1 (Text Box and Folder Browser)
        self.lineInput = QLineEdit(self)
        
        # If there is a link already established, display link path in text box
        if os.path.isdir(dst):
            self.lineInput.setText(os.readlink(dst))
        hLayout1.addWidget(self.lineInput) 

        # "..." Button          
        btn = QPushButton("...")
        btn.pressed.connect(self.browse_folder)
        hLayout1.addWidget(btn)
        
        # Horizontal Box Layout 2 (Cancel or Save)
        btn = QPushButton("MWC Management")
        btn.pressed.connect(self.mwcManagement)
        hLayout2.addWidget(btn)
        
        hLayout2.addStretch()
        btn = QPushButton("Cancel")
        btn.pressed.connect(self.on_cancel_clicked)
        hLayout2.addWidget(btn)
        
        btn = QPushButton("Save")
        btn.pressed.connect(self.on_savebtn_clicked)
        hLayout2.addWidget(btn)
        
        widget = QWidget()
        widget.setLayout(pagelayout)
        self.setCentralWidget(widget)
    
    # Message Box methods
    def SelectBuildPath(self):
        msgBox = QMessageBox()
        msgBox.setWindowIcon(QIcon('runningman.png'))
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.setWindowTitle("ERROR")
        msgBox.setText("Please select a build path")
        msgBox.exec_() 
        
    ### Buttons on Main Window ###
    
    # Browse Folder, AKA "..." button
    def browse_folder(self):
        self.stacklayout.setCurrentIndex(2)
        # Folder that opens with "..." button
        #fname = QFileDialog.getExistingDirectory(self, "Select a directory", "C:/Users/mri/Desktop/FUZE_Builds_2019.2_StagingParent/Builds")
        # For testing comment out above variable, then uncomment below variable and add in desired path to open in QFileDialog
        fname = QFileDialog.getExistingDirectory(self, "Select a directory", "C:/Users/Cody/Desktop/Fuze_Staging_Parent/Builds")
        
        # Displays selected folder from browse into text box  
        if fname:
            self.lineInput.setText(fname)
            
    # Save button     
    def on_savebtn_clicked(self):
        self.stacklayout.setCurrentIndex(0)
        
        # Message Box methods
        def SelectBuildPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("ERROR")
            msgBox.setText("Please select a build path")
            msgBox.exec_()
        def NotVaildPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("ERROR")
            msgBox.setText("This is not a vaild path")
            msgBox.exec_()
        def SuccessfulBuild(self):
            msgBox = QMessageBox()
            msgBox.setWindowTitle("Success")
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setWindowTitle("Successful")
            msgBox.setText("Link successful")
            msgBox.exec_()
        def CurrentPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("Current Path")
            msgBox.setText("This is the current path")
            msgBox.exec_()          
            
        # Check if text box has a path 
        if str(self.lineInput.text()) == "":
            
            #SelectBuildPath(self)
            MainWindow.SelectBuildPath(self)
            
        # Check if text input is a valid path
        elif not os.path.exists(str(self.lineInput.text())):
            NotVaildPath(self)
            
        elif str(self.lineInput.text()) != "":                   
            
            # Run the symbolic link creation code
            def run_symlink():
                
                #dst = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze"
                # For use when testig outside of volume. Add in whatever path you'd like to test at here and comment out the path above
                dst = "C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze"             
                
                def force_symlink(src, dst):
                    try:
                        # Linking source to destination
                        os.symlink(src, dst)
                        
                        # Message box to signify successful link creation
                        SuccessfulBuild(self)
                        fuzeZip.zippingMessage()
                        fuzeZip.zipProgressMessage()
                        fuzeZip.directoryZip()
                        fuzeZip.zipDone()
                        
                    except OSError as e:
                        if os.path.exists(dst):
                            
                            # MessageBoxes
                            def OverWriteSuccess(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowTitle("Success")
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Information)
                                msgBox.setText("Successfully Overwritten!")
                                msgBox.exec_()
                            def NeverReachError(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowTitle("Problem")
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Critical)
                                msgBox.setText("There is an issue, contact the software team")
                                msgBox.exec_()                           
                            def PathOverwrite(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Question)
                                msgBox.setWindowTitle("Build Overwrite")
                                msgBox.setText("This will alter the main build for Fuze")
                                msgBox.setInformativeText("Would you like the overwrite it?")
                                msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                                msgBox.setDefaultButton(QMessageBox.Yes)
                                
                                ret = msgBox.exec_()

                                # Successful link creation                        
                                if ret == QMessageBox.Yes:
                                    os.remove(dst)
                                    os.symlink(src, dst)
                                    OverWriteSuccess(self)
                                    fuzeZip.zippingMessage()
                                    fuzeZip.directoryZip()
                                    fuzeZip.zipDone()                      

                                elif ret == QMessageBox.No:
                                    return
                                
                                # If an error gets to this point, review the code because it should never reach this check
                                else:
                                    NeverReachError(self)
                            
                            # Check if text box has a path 
                            if str(self.lineInput.text()) == "":
                                SelectBuildPath(self)
                
                            # Check if text input is a valid path
                            elif not os.path.exists(str(self.lineInput.text())):
                                NotVaildPath(self)
                                
                            # Ensure text box content isnt' already the current link path
                            elif str(self.lineInput.text()) == str(os.readlink(dst)):
                                CurrentPath(self)
                                
                            elif str(self.lineInput.text()) != str(os.readlink(dst)):
                                if str(self.lineInput.text()) == str(os.readlink(dst)):
                                    CurrentPath(self)
                                
                                elif str(self.lineInput.text()) != str(os.readlink(dst)):
                                    PathOverwrite(self)
                                
                src = str(self.lineInput.text())
                force_symlink(src, dst)       
            run_symlink()
            
    # MWC Management
    def mwcManagement(self):
        self.stacklayout.setCurrentIndex(2)
        self.w = MWCManagementWindow()
        self.w.show()
    
    # Cancels the operation            
    def on_cancel_clicked(self):
        self.stacklayout.setCurrentIndex(3)
        sys.exit(0)
    
    # Running Man Icon
    def setIcon(self):
        appIcon = QIcon("runningman.png")
        self.setWindowIcon(appIcon)

# Main
if __name__=='__main__': 
    if not is_admin():  
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)
    
    else:
        app = QApplication(sys.argv)         
        window = MainWindow()
        window.show()
        app.exec_()