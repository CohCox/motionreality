""" 
FUZE BUILD SELECTOR for use with Dauntless Fuze
Bug where after creation of link, the warning of the path existing does not show up in you select same folder again
"""
##To Create .exe
#       1.from the terminal cd into the same directory this script is in
#       2.then type pyinstaller --onedir --windowed --icon=run.ico FuzeBuildSelector.py
#         --icon=run.ico is optional to give the application an icon, place the runningman.png into the folder holding the .exe to have the window icon
#         Utilize --onedir instead of --onefile as it greatly increases the start up speed of the application, trials averaged 15 seconds to start up with --onefile vs 4 seconds with --onedir 
import sys, ctypes, os, errno, getpass, zipfile, paramiko, tqdm, time
from paramiko import SSHClient, SFTPClient
from scp import SCPClient
from os.path import basename
import zip 

from PySide2.QtCore import Qt, QSize, Signal
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (
    QApplication,
    QHBoxLayout,
    QFormLayout,
    QLabel,
    QMainWindow,
    QPushButton,
    QStackedLayout,
    QVBoxLayout,
    QWidget,
    QFileDialog,
    QLineEdit,
    QMessageBox,
    QComboBox,
    QFrame,
    QCheckBox,
)

#Initial Variables
mwcNumbers = {'MWC1':"192.168.254.67",
            'MWC2': '192.168.254.68'
            }
localDirec = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze.zip'
remoteDirec = '/Users/CohCox/Desktop/fuze'
zipRemote = remoteDirec + "/fuze.zip"

# Check if admin
def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False
    
class MWCManagementWindow(QWidget):
    def __init__(self):
        super().__init__()
        
        self.mwcNumbers = mwcNumbers
        
        self.setWindowTitle("MWC Management")
        self.setFixedSize(400, 150)
        self.pageLayout = QHBoxLayout()
        self.vLayout1 = QVBoxLayout()
        self.vLayout2 = QVBoxLayout()
        self.pageLayout.addLayout(self.vLayout1)
        self.pageLayout.addLayout(self.vLayout2)
        
        self.vLayout1.addStretch()
        
        self.label = QLabel("Choose MWCs")
        self.vLayout1.addWidget(self.label)
        
        # MWC Checkboxes
        self.checkboxes = {}
        for label in mwcNumbers:
            checkbox = QCheckBox(label)
            checkbox.setCheckState(Qt.Unchecked)
            checkbox.stateChanged.connect(self.show_state)
            self.vLayout1.addWidget(checkbox)
            self.checkboxes[label] = checkbox
        
        self.vLayout1.addStretch()
        self.vLayout2.addStretch()
        
        # Buttons
        btn = QPushButton("Select All")
        btn.pressed.connect(self.select_all_pressed)
        self.vLayout2.addWidget(btn)
        
        btn = QPushButton("Clear Selections")
        btn.pressed.connect(self.deselect_all_pressed)
        self.vLayout2.addWidget(btn)
        
        btn = QPushButton("Push to MWCs")
        btn.pressed.connect(self.push_to_mwc_pressed)
        self.vLayout2.addWidget(btn)  
        self.vLayout2.addStretch()
        
        self.setLayout(self.pageLayout)
        
    def show_state(self, s):
        print(s == Qt.Checked)
        print(s)
    def select_all_pressed(self):
        for label, checkbox in self.checkboxes.items():
            checkbox.setCheckState(Qt.Checked)
    def deselect_all_pressed(self):
        for label, checkbox in self.checkboxes.items():
            checkbox.setCheckState(Qt.Unchecked)
    def push_to_mwc_pressed(self):
        # for loop to run through checkboxes 
        # set bool to true if checked
        # if bool is false cause warning box
        # Check if you can ping the ip
        # Do prepwork (direcZip) here
        for label, checkbox in self.checkboxes.items():
            if checkbox.checkState() and label in mwcNumbers:
                ip = mwcNumbers[label]
                if ip == '192.168.254.68':
                    ip = '192.168.254.68'
                    username = 'CohCox'
                    pswrd = '1welcome2'
                    
                    RunSCP()
                else:
                    print("Not Connected")

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        
        # Window Title and Icon
        self.setWindowTitle("Fuze Build Selector")
        self.setFixedSize(QSize(400, 90))        
        self.setIcon()
        
        # Basic Page Layout
        pagelayout = QVBoxLayout()
        hLayout0 = QHBoxLayout()
        hLayout1 = QHBoxLayout()
        hLayout2 = QHBoxLayout()
        hLayout3 = QHBoxLayout()    
        pagelayout.addLayout(hLayout0)
        pagelayout.addLayout(hLayout1)
        pagelayout.addLayout(hLayout2)
        self.stacklayout = QStackedLayout()  
        pagelayout.addLayout(self.stacklayout)
        
        # Hard-coded destination path for link, MUST ALTER FOR SPECIFC SYSTEM
        #This is the REAL path!!!! Uncomment this when moved to the release volume! Will also need to be changed under run_symlink method!!! Line 163
        #dst = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze"
        # For use when testig outside of volume. Add in whatever path you'd like to test at here and comment out the path above
        dst = "C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze"
        
        # Horizontal Box Layout 0 (Text Label)
        label = QLabel("Select A Build")
        hLayout0.addWidget(label)
        
        # Horizontal Box Layout 1 (Text Box and Folder Browser)
        self.lineInput = QLineEdit(self)
        
        # If there is a link already established, display link path in text box
        if os.path.isdir(dst):
            self.lineInput.setText(os.readlink(dst))
        hLayout1.addWidget(self.lineInput) 

        # "..." Button          
        btn = QPushButton("...")
        btn.pressed.connect(self.browse_folder)
        hLayout1.addWidget(btn)
        
        # Horizontal Box Layout 2 (Cancel or Save)
        btn = QPushButton("MWC Management")
        btn.pressed.connect(self.mwcManagement)
        hLayout2.addWidget(btn)
        
        hLayout2.addStretch()
        btn = QPushButton("Cancel")
        btn.pressed.connect(self.on_cancel_clicked)
        hLayout2.addWidget(btn)
        
        btn = QPushButton("Save")
        btn.pressed.connect(self.on_savebtn_clicked)
        hLayout2.addWidget(btn)
        
        widget = QWidget()
        widget.setLayout(pagelayout)
        self.setCentralWidget(widget)
    
    # Message Box methods
    def SelectBuildPath(self):
        msgBox = QMessageBox()
        msgBox.setWindowIcon(QIcon('runningman.png'))
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.setWindowTitle("ERROR")
        msgBox.setText("Please select a build path")
        msgBox.exec_() 
        
    ### Buttons on Main Window ###
    
    # Browse Folder, AKA "..." button
    def browse_folder(self):
        self.stacklayout.setCurrentIndex(2)
        # Folder that opens with "..." button
        #fname = QFileDialog.getExistingDirectory(self, "Select a directory", "C:/Users/mri/Desktop/FUZE_Builds_2019.2_StagingParent/Builds")
        # For testing comment out above variable, then uncomment below variable and add in desired path to open in QFileDialog
        fname = QFileDialog.getExistingDirectory(self, "Select a directory", "C:/Users/Cody/Desktop/Fuze_Staging_Parent/Builds")
        
        # Displays selected folder from browse into text box  
        if fname:
            self.lineInput.setText(fname)
            
    # Save button     
    def on_savebtn_clicked(self):
        self.stacklayout.setCurrentIndex(0)
        
        # Message Box methods
        def SelectBuildPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("ERROR")
            msgBox.setText("Please select a build path")
            msgBox.exec_()
        def NotVaildPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("ERROR")
            msgBox.setText("This is not a vaild path")
            msgBox.exec_()
        def SuccessfulBuild(self):
            msgBox = QMessageBox()
            msgBox.setWindowTitle("Success")
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setWindowTitle("Successful")
            msgBox.setText("Link successful")
            msgBox.exec_()
        def CurrentPath(self):
            msgBox = QMessageBox()
            msgBox.setWindowIcon(QIcon('runningman.png'))
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setWindowTitle("Current Path")
            msgBox.setText("This is the current path")
            msgBox.exec_()          
            
        # Check if text box has a path 
        if str(self.lineInput.text()) == "":
            
            #SelectBuildPath(self)
            MainWindow.SelectBuildPath(self)
            
        # Check if text input is a valid path
        elif not os.path.exists(str(self.lineInput.text())):
            NotVaildPath(self)
            
        elif str(self.lineInput.text()) != "":                   
            
            # Run the symbolic link creation code
            def run_symlink():
                
                #dst = "C:/Users/Mri/Desktop/FUZE_Builds_2019.2_StagingParent/fuze"
                # For use when testig outside of volume. Add in whatever path you'd like to test at here and comment out the path above
                dst = "C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze"             
                
                def force_symlink(src, dst):
                    try:
                        # Linking source to destination
                        os.symlink(src, dst)
                        
                        # Message box to signify successful link creation
                        SuccessfulBuild(self)
                        
                    except OSError as e:
                        if os.path.exists(dst):
                            
                            # MessageBox Methods
                            def OverWriteSuccess(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowTitle("Success")
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Information)
                                msgBox.setText("Successfully Overwritten!")
                                msgBox.exec_()
                            def NeverReachError(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowTitle("Problem")
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Critical)
                                msgBox.setText("There is an issue, contact the software team")
                                msgBox.exec_()                           
                            def PathOverwrite(self):
                                msgBox = QMessageBox()
                                msgBox.setWindowIcon(QIcon('runningman.png'))
                                msgBox.setIcon(QMessageBox.Question)
                                msgBox.setWindowTitle("Build Overwrite")
                                msgBox.setText("This will alter the main build for Fuze")
                                msgBox.setInformativeText("Would you like the overwrite it?")
                                msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                                msgBox.setDefaultButton(QMessageBox.Yes)
                                
                                ret = msgBox.exec_()

                                # Successful link creation                        
                                if ret == QMessageBox.Yes:
                                    os.remove(dst)
                                    os.symlink(src, dst)
                                    OverWriteSuccess(self)                       

                                elif ret == QMessageBox.No:
                                    return
                                
                                # If an error gets to this point, review the code because it should never reach this check
                                else:
                                    NeverReachError(self)
                            
                            # Check if text box has a path 
                            if str(self.lineInput.text()) == "":
                                SelectBuildPath(self)
                
                            # Check if text input is a valid path
                            elif not os.path.exists(str(self.lineInput.text())):
                                NotVaildPath(self)
                                
                            # Ensure text box content isnt' already the current link path
                            elif str(self.lineInput.text()) == str(os.readlink(dst)):
                                CurrentPath(self)
                                
                            elif str(self.lineInput.text()) != str(os.readlink(dst)):
                                if str(self.lineInput.text()) == str(os.readlink(dst)):
                                    CurrentPath(self)
                                
                                elif str(self.lineInput.text()) != str(os.readlink(dst)):
                                    PathOverwrite(self)
                                
                src = str(self.lineInput.text())
                force_symlink(src, dst)       
            run_symlink()
            
    
    # MWC Management
    def mwcManagement(self):
        self.stacklayout.setCurrentIndex(2)
        #print("Button Pressed")
        self.w = MWCManagementWindow()
        self.w.show()
    
    # Cancels the operation            
    def on_cancel_clicked(self):
        self.stacklayout.setCurrentIndex(3)
        sys.exit(0)
    
    # Running Man Icon
    def setIcon(self):
        appIcon = QIcon("runningman.png")
        self.setWindowIcon(appIcon)

### Functions for program functionality ###

#Performs Copy of build over SSH to remote destination
def RunSCP():
            
    #Run zipping function to get a fuze.zip
    msgBox = QMessageBox()
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setWindowTitle("Zip File")
    msgBox.setText("Zipping up build...")
    msgBox.show()
    directoryZip()
    msgBox.close()
    
    msgBox = QMessageBox()
    msgBox.setText("Zip Complete, Continue with copy?")
    msgBox.show()
    msgBox.exec_()
    #Set up SSH client into remote destination
    ssh = SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(hostname=pi, username=username, password=pswrd)
        sftp = ssh.open_sftp()
        sftp.chdir(remoteDirec)
        
        #Function for progress status
        def printTotals(transferred, toBeTransferred):
                print("Transferred: {0}\tOut of: {1}".format(transferred, toBeTransferred))
        def buildCopy():
            print("Copying over build...")
            sftp.put(localDirec, zipRemote, callback=printTotals)      
            print("Unzipping file...")
            ssh.exec_command("unzip /Users/CohCox/Desktop/fuze/fuze.zip -d /Users/CohCox/Desktop/fuze")
            rooby = input("> ")
            ssh.exec_command("mv /Users/CohCox/Desktop/fuze/Fuze_Staging_Parent/fuze /Users/CohCox/Desktop/fuze")
            rooby = input("> ")
            ssh.exec_command("rm -r /Users/CohCox/Desktop/fuze/Fuze_Staging_Parent")
            rooby = input("> ")
            sftp.remove(zipRemote)
            
            msgBox = QMessageBox()
            msgBox.setText("Build Copy Complete!")
            msgBox.show()
            msgBox.exec_()

        #Check if zip file already exists on pack, if so delete it
        try:
            print(sftp.stat(zipRemote))
            sftp.remove(zipRemote)
            buildCopy()
        
        except:
            pass
        
        #Check if unzipped build folder already present on remote destination, if so delete and proceed THIS DOES NOT FUNCTION YET!!!!
        try:
            totalPath = remoteDirec + "/fuze"
            print(sftp.stat(totalPath))
            ssh.exec_command("rm -r /Users/CohCox/Desktop/fuze/fuze")
            
            buildCopy()
                
        #If no issues, copies zip file to remote destination, unzips and then deletes the zip file
        except IOError:
            buildCopy()
            
        sftp.close()
        ssh.close()
    except paramiko.SSHException:
        print('Connection Error')
        chubby = input("Press any key to close...")
    sftp.close()

#Zipping Function 
def directoryZip():
    output_filename = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze.zip'
    dir_name = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze'
    
    if not os.path.exists(dir_name):
        print("Fuze directory does not exist, make a link butthole!")
        
    #Check if zipfile already exists locally
    if not os.path.exists(output_filename):
        #os.remove('C:/Users/Cody/Desktop/FuzeBuildSelector/testwithSSH/fuze.zip')
        print("Zipping up build...")
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))
        
    #Begin zipping process
    elif os.path.exists(output_filename): 
        os.remove(output_filename)
        print("Zipping up build...")
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))

# Main
if __name__=='__main__': 
    if not is_admin():  
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)
    
    else:
        app = QApplication(sys.argv)         
        window = MainWindow()
        window.show()
        app.exec_()