### This creates a build folder into a zip, then securely sends a copy of that zip over to a remote destination, unzips it and then deletes the zip file ###

### Need to add in a few more edge cases to ensure security
### Bug where zipping the desired directory will zip the entire path instead of just the desired directory

import sys, ctypes, os, errno, getpass, zipfile, paramiko, tqdm, time
from paramiko import SSHClient, SFTPClient
from scp import SCPClient
from os.path import basename

#Initial Variables
localDirec = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze.zip'
remoteDirec = '/home/pi/Desktop/fuze'
zipRemote = remoteDirec + "/fuze.zip"

# Check if admin
def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False
    
#Performs Copy of build over SSH to remote destination
def RunSCP():
    
    #Run zipping function to get a fuze.zip
    directoryZip()
    shooby = input("> ")
    #Set up SSH client into remote destination
    ssh = SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(hostname="192.168.254.67", username="pi", password="1welcome2")
        sftp = ssh.open_sftp()
        sftp.chdir(remoteDirec)
        
        #Function for progress status
        def printTotals(transferred, toBeTransferred):
                print("Transferred: {0}\tOut of: {1}".format(transferred, toBeTransferred))
        def buildCopy():
            print("Copying over build...")
            sftp.put(localDirec, zipRemote, callback=printTotals)      
            print("Unzipping file...")
            ssh.exec_command("unzip /home/pi/Desktop/fuze/fuze.zip -d /home/pi/Desktop/fuze")
            shooby = input("> ")
            ssh.exec_command("mv /home/pi/Desktop/fuze/Fuze_Staging_Parent/fuze /home/pi/Desktop/fuze")
            shooby = input("> ")
            ssh.exec_command("rm -r /home/pi/Desktop/fuze/Fuze_Staging_Parent")
            shooby = input("> ")
            sftp.remove(zipRemote)

        #Check if zip file already exists on pack, if so delete it
        try:
            print(sftp.stat(zipRemote))
            sftp.remove(zipRemote)
            buildCopy()
        
        except:
            pass
        
        #Check if unzipped build folder already present on remote destination, if so delete and proceed THIS DOES NOT FUNCTION YET!!!!
        try:
            totalPath = remoteDirec + "/fuze"
            print(sftp.stat(totalPath))
            ssh.exec_command("rm -r /home/pi/Desktop/fuze/fuze")
            
            buildCopy()
                
        #If no issues, copies zip file to remote destination, unzips and then deletes the zip file
        except IOError:
            buildCopy()
            
        sftp.close()
        ssh.close()
    except paramiko.SSHException:
        print('Connection Error')
        chubby = input("Press any key to close...")
    sftp.close()

#Zipping Function 
def directoryZip():
    output_filename = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze.zip'
    dir_name = 'C:/Users/Cody/Desktop/Fuze_Staging_Parent/fuze'
    
    #Check if zipfile already exists locally
    if not os.path.exists(output_filename):
        #os.remove('C:/Users/Cody/Desktop/FuzeBuildSelector/testwithSSH/fuze.zip')
        print("Zipping up build...")
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))
        
    #Begin zipping process
    elif os.path.exists(output_filename): 
        os.remove(output_filename)
        print("Zipping up build...")
        zf = zipfile.ZipFile(output_filename, "w")
        for dirname, subdirs, files in os.walk(dir_name):
            zf.write(os.path.relpath(dirname))
            for filename in files:
                zf.write(os.path.join(os.path.relpath(dirname), filename))

# Main
if __name__ == '__main__':
    
    if not is_admin():  
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)
    
    else:
        RunSCP()
